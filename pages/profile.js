import { makeStyles } from '@material-ui/core/styles';
import Intro from '../src/components/intro';
import Nav from '../src/components/nav';
import Portrait from '../src/components/portrait';
import Contact from '../src/components/contact';
import Projects from '../src/components/projects';
import { urlObjectKeys } from 'next/dist/next-server/lib/utils';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    paddingBottom: 30,
  },

  jumbotron: {
    height: 400,
    width: '100%',
    background: 'lightGrey',
    overflow: 'hidden',
    backgroundImage: 'url(img.jpeg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  photo: {
    height: 300,
    width: 300,
    borderRadius: '50%',
    position: 'absolute',
    marginTop: 300,
    border: '5px solid white',
    overflow: 'hidden',
  },
  picture: {
    maxWidth: '100%',
  },
}));
const Profile = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Nav name='Anggiela Dorador' />
      <div className={classes.jumbotron}></div>
      <Portrait />
      <Contact />
      <Intro />
      <Projects></Projects>
    </div>
  );
};
export default Profile;
