import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Chip from "@material-ui/core/Chip";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    flexWrap: "wrap",
    listStyle: "none",
    padding: theme.spacing(0.5),
    margin: 0,
  },

  html: {
    backgroundColor: "#F2542D",
    margin: theme.spacing(0.5),
    color: "white",
  },
  react: {
    backgroundColor: "#00CECB",
    margin: theme.spacing(0.5),
    color: "white",
  },
  js: {
    backgroundColor: "#ffb30f",
    margin: theme.spacing(0.5),
    color: "white",
  },
  nodejs: {
    backgroundColor: "#29BF12",
    margin: theme.spacing(0.5),
    color: "white",
  },
  flexbox: {
    backgroundColor: "#d9d0de",
    margin: theme.spacing(0.5),
    color: "white",
  },
}));

const Chips = (Props) => {
  const classes = useStyles();
  switch (Props.name) {
    case "HTML":
      return <Chip label={Props.name} className={classes.html} />;
      break;
    case "REACTJS":
      return <Chip label={Props.name} className={classes.react} />;
      break;
    case "JS":
      return <Chip label={Props.name} className={classes.js} />;
      break;
    case "NODEJS":
      return <Chip label={Props.name} className={classes.nodejs} />;
      break;
    default:
      return <Chip label={Props.name} className={classes.flexbox} />;
      break;
  }
};
export default Chips;
