import LinkedInIcon from '@material-ui/icons/LinkedIn';
import Link from 'next/link';
import { makeStyles } from '@material-ui/core/styles';
import InstagramIcon from '@material-ui/icons/Instagram';
import GitHubIcon from '@material-ui/icons/GitHub';
import CodeIcon from '@material-ui/icons/Code';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    marginTop: 150,
  },
  icon: {
    color: '#699df2',
  },
}));

const Contact = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Link href='https://www.linkedin.com/in/anggiela-dorador/'>
        <a target='_blank'>
          <LinkedInIcon fontSize='large' className={classes.icon} />
        </a>
      </Link>

      <Link href='https://www.instagram.com/bridzkey/'>
        <a target='_blank'>
          <InstagramIcon fontSize='large' className={classes.icon} />
        </a>
      </Link>

      <Link href='https://github.com/anggieladorador'>
        <a target='_blank'>
          <GitHubIcon fontSize='large' className={classes.icon} />
        </a>
      </Link>

      <Link href='https://repl.it/@anggieladorador'>
        <a target='_blank'>
          <CodeIcon fontSize='large' className={classes.icon} />
        </a>
      </Link>
    </div>
  );
};
export default Contact;
