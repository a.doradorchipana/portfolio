import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useScrollTrigger } from '@material-ui/core';

const useStyles = makeStyles({
  container: {
    width: 815,
    border: '3px solid #699df2',
    marginTop: 10,
    borderRadius: 10,
  },
  title: {
    fontSize: 18,
  },
});

const Intro = () => {
  const classes = useStyles();
  return (
    <Card className={classes.container}>
      <CardContent>
        <Typography className={classes.title} color='textPrimary' gutterBottom>
          Sobre mi
        </Typography>
        <Typography variant='body1' align='justify'>
          Hola! Mi nombre es Anggiela Dorador y soy egresada de ingeniería en
          informática, durante mi vida universitaría me he destacado
          principalmente por mi participación en actividades extracurriculares
          que estaban relacionadas con la carrera, como por ejemplo asistir y
          colaborar con algunos talleres realizados en INACAP como Mujeres en
          las TIC, participando en la competencia de "Los genios no duermen" y
          presentando la carrera a estudiantes nuevos. Me caracterizo por ser
          entusiasta, perseverante y agradable{' '}
        </Typography>
        <Typography>
          <span style={{ fontSize: '30px' }}>&#127918;</span> Mis hobbies:
          <ul>
            <li>
              Juegos de mesa, el último juego que jugué fue "la isla prohibida"
            </li>
            <li>Leer, entre mis últimas lecturas está Attachements y Carrie</li>
            <li>
              Ilustración, me encanta dibujar con diferentes técnicas entre
              ellas goache, acuarelas y también en digital.
            </li>
            <li>
              Video juegos, entre mis juegos favoritos está Dead Space, Link's
              awakening, Donkey Kong y Hollow Knight
            </li>
          </ul>
        </Typography>
      </CardContent>
    </Card>
  );
};

export default Intro;
