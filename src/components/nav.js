import { AppBar, Toolbar, IconButton, Typography } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  app: {
    backgroundColor: '#699df2',
  },
}));

const Nav = (Props) => {
  const classes = useStyles();
  return (
    <AppBar position='static' className={classes.app}>
      <Toolbar>
        <Typography variant='h6' className={classes.title}>
          {Props.name}
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Nav;
