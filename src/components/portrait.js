import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  photo: {
    height: 300,
    width: 300,
    borderRadius: '50%',
    position: 'absolute',
    marginTop: 300,
    border: '5px solid white',
    overflow: 'hidden',
  },
  picture: {
    maxWidth: '100%',
  },
}));

const Portrait = (Props) => {
  const classes = useStyles();
  return (
    <div className={classes.photo}>
      <img
        className={classes.picture}
        src='myself.jpeg'
        alt='myself in a picture'
      />
    </div>
  );
};
export default Portrait;
