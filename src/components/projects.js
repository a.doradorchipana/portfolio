import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import Link from "next/link";
import Badges from "./badges";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row",
    width: "100%",
    justifyContent: "center",
    marginTop: 150,
    justifyContent: "space-evenly",
  },
  cards: {
    maxWidth: 345,
    width: 300,
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  button: {
    width: "100%",
    color: "#FFFFFF",
    backgroundColor: "#699df2",
    "&:hover": {
      backgroundColor: "#8eb6f5",
    },
  },
  content: {
    height: 100,
  },
}));
const Projects = () => {
  const classes = useStyles();
  const projects = [
    {
      name: "Lista de tareas",
      descripcion: "Lista de tareas hechas con ReactJS y Bootstrap",
      img: "lista.jpeg",
      link: "https://todolistanniesite.netlify.app",
      badges: ["REACTJS", "FLEXBOX"],
    },
    {
      name: "Calculadora JS",
      descripcion:
        "Calculadora hecha con Vanilla JS, tanto el dom como su funcionamiento",
      img: "calculadora.jpeg",
      link: "https://calculadorajs.anggieladorador.repl.co/",
      badges: ["HTML", "CSS", "JS"],
    },
    {
      name: "coordandtemp",
      descripcion:
        "aplicación de consola, se conecta a mapbox para obtener coordenadas de la ciudad a consultar y a openweather para consultar la temperatura actual",
      img: "coordandtemp.jpeg",
      link: "https://repl.it/@anggieladorador/coordandtemp",
      badges: ["NODEJS", "JS"],
    },
  ];

  return (
    <div className={classes.container}>
      {projects.map((p, index) => {
        return (
          <Card className={classes.cards} key={index}>
            <CardHeader align="center" title={p.name} />
            <CardMedia className={classes.media} image={p.img} title={p.name} />
            <CardContent className={classes.content}>
              <Typography variant="body2" color="textSecondary" component="p">
                {p.descripcion}
              </Typography>
            </CardContent>
            <CardContent>
              {p.badges.map((b, index) => {
                return <Badges name={b} key={index} />;
              })}
            </CardContent>
            <CardActions>
              <Link href={p.link}>
                <Button
                  variant="contained"
                  target="_blank"
                  className={classes.button}
                >
                  ver más
                </Button>
              </Link>
            </CardActions>
          </Card>
        );
      })}
    </div>
  );
};
export default Projects;
